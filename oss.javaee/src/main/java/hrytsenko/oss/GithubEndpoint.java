package hrytsenko.oss;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import hrytsenko.oss.ext.JoltTransformer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class GithubEndpoint {

    private static final String REPOS_API = "https://api.github.com/users/{username}/repos";
    private static final String REPOS_SPEC = "/github/repositories_spec.json";

    private Client client;

    @PostConstruct
    public void initClient() {
        client = ClientBuilder.newClient();
    }

    public List<Project> findProjects(String username) {
        log.info("Find projects for {}.", username);

        WebTarget target = client.target(REPOS_API).resolveTemplate("username", username);
        target.register(new JoltTransformer(REPOS_SPEC));

        return target.request().accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.USER_AGENT, GithubEndpoint.class.getName()).get()
                .readEntity(new GenericType<List<Project>>() {
                });
    }

}
