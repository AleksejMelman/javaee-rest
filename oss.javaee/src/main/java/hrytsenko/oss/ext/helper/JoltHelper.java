package hrytsenko.oss.ext.helper;

import com.bazaarvoice.jolt.Chainr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.experimental.UtilityClass;

@UtilityClass
public class JoltHelper {

    private final Gson GSON = new GsonBuilder().create();

    public String transform(Chainr jolt, String inputJson) {
        Object input = GSON.fromJson(inputJson, Object.class);
        Object output = jolt.transform(input);
        return GSON.toJson(output);
    }

}
