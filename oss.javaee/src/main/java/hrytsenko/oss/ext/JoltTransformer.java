package hrytsenko.oss.ext;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;

import org.apache.commons.io.IOUtils;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.ChainrFactory;

import hrytsenko.oss.ext.helper.JoltHelper;

public class JoltTransformer implements ReaderInterceptor {

    private Chainr jolt;

    public JoltTransformer(String spec) {
        jolt = ChainrFactory.fromClassPath(spec);
    }

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException {
        String inputJson = IOUtils.toString(context.getInputStream(), StandardCharsets.UTF_8);
        String outputJson = JoltHelper.transform(jolt, inputJson);
        context.setInputStream(new ByteArrayInputStream(outputJson.getBytes(StandardCharsets.UTF_8)));
        return context.proceed();
    }

}
