package hrytsenko.oss.ext.helper;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CsvHelper {

    public void write(Object entity, Type entityType, OutputStream entityStream) throws IOException {
        CsvMapper mapper = new CsvMapper();
        mapper.writer().with(schemaFor(entityType).withHeader()).writeValue(entityStream, entity);
    }

    private CsvSchema schemaFor(Type entityType) {
        CsvSchema.Builder builder = new CsvSchema.Builder();
        Field[] fields = getActualEntity(entityType).map(Class::getDeclaredFields)
                .orElseThrow(() -> new IllegalStateException("Unsupported type."));
        Arrays.stream(fields).filter(CsvHelper::isInstanceField).map(Field::getName).forEach(builder::addColumn);
        return builder.build();
    }

    private boolean isInstanceField(Field field) {
        return !Modifier.isStatic(field.getModifiers());
    }

    public Optional<Class<?>> getActualEntity(Type entityType) {
        if (entityType instanceof Class) {
            return Optional.of((Class<?>) entityType);
        }

        if (entityType instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) entityType;
            if (Collection.class.isAssignableFrom((Class<?>) parameterizedType.getRawType())) {
                return Optional.of((Class<?>) parameterizedType.getActualTypeArguments()[0]);
            }
        }

        return Optional.empty();
    }

}
