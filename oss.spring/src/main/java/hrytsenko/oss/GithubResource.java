package hrytsenko.oss;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.Setter;

@RestController
@RequestMapping(path = "github")
public class GithubResource {

    @Setter(onMethod = @__(@Inject))
    private GithubEndpoint endpoint;

    @RequestMapping(path = "{username}/projects", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE, "text/csv" })
    public List<Project> findProjects(@PathVariable("username") String username) {
        return endpoint.findProjects(username);
    }

}
