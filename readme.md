# About

Examples of RESTful web service implementation using different technologies.

This service returns the list of open-source projects from [Github] as JSON or CSV.

| Module       | Platform  | Server       |
|--------------|-----------|--------------|
| `oss.javaee` | [Java EE] | [WildFly]    |
| `oss.spring` | [Spring]  | [Undertow]   |
| `oss.camel`  | [Camel]   | [ServiceMix] |

# Java EE

On the Java EE platform, we use the [JAX-RS] technology for implementing RESTful web services.

Initially, we define the JAX-RS application.

```java
@ApplicationPath("")
public class App extends Application {...}
```

Then, we define the resource for GitHub.

```java
@Path("github")
public class GithubResource {...}
```

In this resource, we define the sub-resource that returns the projects for the specific user.

```java
@GET
@Path("{username}/projects")
@Produces({ MediaType.APPLICATION_JSON, "text/csv" })
public List<Project> findProjects(@PathParam("username") String username) {...}
```

For this sub-resource, we use the `@Produces` annotation to specify that this resource supports JSON and CSV.

Typically, the Java EE servers provide support for the JSON format out of the box.
Otherwise, you can use [Jackson JAX-RS].

And, we implement an entity provider for CSV.
This provider takes any Java object and transforms it into the CSV document using [Jackson CSV].

```java
@Provider
@Produces("text/csv")
public class CsvWriter implements MessageBodyWriter<Object> {...}
```

We use the `@Provider` annotation to register this provider in the JAX-RS application.
Then, we use the `@Produces` annotation to specify the supported content type.
So, the JAX-RS application will use this provider for requests that accept the `text/csv` media type.

### Client

To request data from GitHub we use the JAX-RS client.
For each request, we specify the GitHub resource.
Then, we register the interceptor of the response.
This interceptor transforms the response using [Jolt].

```java
WebTarget target = client.target(REPOS_API).resolveTemplate("username", username);
target.register(new JoltTransformer(REPOS_SPEC));
```

Then, we send the `GET` request to the GitHub resource.
At last, we convert the response entity into the Java object.

```java
return target.request().accept(MediaType.APPLICATION_JSON)
        .header(HttpHeaders.USER_AGENT, GithubEndpoint.class.getName()).get()
        .readEntity(new GenericType<List<Project>>() {
        });
```

## Deploy

You can use the WildFly server:

1. Download WildFly (Full & Web Distribution).
1. Unzip the server.
1. Run `bin\standalone.bat` to start the server.
1. Run `app-deploy.bat` to deploy the application.
1. Run `app-test.bat` to test the application.

Or, you can use the Payara MicroProfile server:

1. Download Payara MicroProfile.
1. Run `app-build.bat` to build the application.
1. Run `java -jar payara-microprofile.jar --deploy oss.war` to start the server and deploy the application.

Also, you can use any other application server.

# Spring

In the Spring framework, we use the Web MVC framework for implementing RESTful web services.

Initially, we create the Web MVC application.
This application is configured to scan components.

```java
@Configuration
@EnableWebMvc
@ComponentScan
public static class Config extends WebMvcConfigurerAdapter {...}
```

The Web MVC framework does not support the automatic discovery of extensions.
Therefore, we should explicitly configure message converters for JSON and CSV.

```java
public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.add(new MappingJackson2HttpMessageConverter());
    converters.add(new CsvWriter());
}
```

Then, we define the resource for GitHub.

```java
@RestController
@RequestMapping(path = "github")
public class GithubResource {...}
```

In this resource, we define the request handler that returns the projects for the specific user.

```java
@RequestMapping(path = "{username}/projects", method = RequestMethod.GET, produces = {
        MediaType.APPLICATION_JSON_VALUE, "text/csv" })
public List<Project> findProjects(@PathVariable("username") String username) {
    return endpoint.findProjects(username);
}
```

We use the `produces` property to specify that this resource supports JSON and CSV.

The Spring framework provides the message converter for JSON.
And, we implement the message converter for CSV.
This converter takes any Java object and transforms it into the CSV document using [Jackson CSV].
We implement the `GenericHttpMessageConverter` interface to support objects of a generic types.

```java
public class CsvWriter implements GenericHttpMessageConverter<Object> {...}
```

We implement the `getSupportedMediaTypes` method to specify the list of supported media types.
So, the Web MVC framework will use this provider for requests that accept the `text/csv` media type.

```java
public static final MediaType TEXT_CSV = MediaType.valueOf("text/csv");

@Override
public List<MediaType> getSupportedMediaTypes() {
    return Arrays.asList(TEXT_CSV);
}
```

### Client

To request data from GitHub we use the `RestTemplate` class.
For each request, we create a new template.
Then, we register the interceptor for the response.
This interceptor transforms the response using [Jolt].

```java
RestTemplate client = new RestTemplate();
client.setInterceptors(Arrays.asList(new JoltTransformer(REPOS_SPEC)));
```

Then, we send the `GET` request to the GitHub resource.
At last, we convert the response entity into the Java object.

```java
return client.exchange(new RequestEntity<>(headers(), HttpMethod.GET, repositoriesFor(username)),
        new ParameterizedTypeReference<List<Project>>() {
        }).getBody();
```

We use the `HttpHeaders` class to configure header fields.

```java
private HttpHeaders headers() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
    headers.add(HttpHeaders.USER_AGENT, GithubEndpoint.class.getName());
    return headers;
}
```

And, we use the `UriComponentsBuilder` class to configure the resource URI.

```java
private URI repositoriesFor(String username) {
    return UriComponentsBuilder.fromHttpUrl(REPOS_API).buildAndExpand(username).toUri();
}
```

## Deploy

You can use the Undertow server:

1. Download WildFly (Servlet-Only Distribution).
1. Unzip the server.
1. Run `bin\standalone.bat` to start the server.
1. Run `app-deploy.bat` to deploy the application.
1. Run `app-test.bat` to test the application.

Also, you can use any other servlet container.

# Camel

We use the [JAX-RS] technology to implement RESTful web services that can be integrated with Camel.
Namely, we use the [Camel CXF] component.

Initially, we define the endpoint for the JAX-RS application.
For this application, we configure providers for JSON and CSV.

```xml
<cxf:rsServer id="ossGithub" address="http://0.0.0.0:8080/oss" serviceClass="hrytsenko.oss.GithubResource">
    <cxf:providers>
        <bean class="com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider" />
        <bean class="hrytsenko.oss.ext.CsvWriter" />
    </cxf:providers>
</cxf:rsServer>
```

We associate this endpoint with the resource for GitHub.
So, the CXF will handle requests to this resource.

```java
@Path("github")
public interface GithubResource {...}
```

In this resource, we define the sub-resource that returns the projects for the specific user.

```java
@GET
@Path("{username}/projects")
@Produces({ MediaType.APPLICATION_JSON, "text/csv" })
List<Project> findProjects(@PathParam("username") String username);
```

For this sub-resource, we use the `@Produces` annotation to specify that this resource supports JSON and CSV.

We use the [Camel Jackson] component for JSON.
And, we implement an entity provider for CSV.
This provider takes any Java object and transforms it into the CSV document using [Jackson CSV].

```java
@Produces("text/csv")
public class CsvWriter implements MessageBodyWriter<Object> {...}
```

We use the `@Produces` annotation to specify the supported content type.
So, the JAX-RS application will use this provider for requests that accept the `text/csv` media type.

### Client

We use the `DispatchRequest` route to forward incoming requests.

```java
public void configure() {
    from(ENDPOINT).toD("direct:${header.operationName}").end();
}
```

And, we use the `FindProjects` route to request data from GitHub.

```java
public void configure() {
    from(URI).setHeader(Exchange.HTTP_METHOD).simple(HttpMethod.GET).setHeader(Exchange.HTTP_PATH).simple(REPOS_URI)
            .removeHeaders("*", Exchange.HTTP_METHOD, Exchange.HTTP_PATH).setHeader(HttpHeaders.ACCEPT)
            .simple(MediaType.APPLICATION_JSON).setHeader(HttpHeaders.USER_AGENT)
            .simple(FindProjects.class.getName()).to(GITHUB).setHeader(JoltConstants.JOLT_RESOURCE_URI)
            .simple(REPOS_SPEC).to(JOLT).unmarshal(new ListJacksonDataFormat(Project.class));
}
```

For each request, we set header fields.
Then, we send this request to GitHub using the CXFRS endpoint.

```xml
<cxf:rsClient id="github" address="https://api.github.com/" />
```

After that, we transform the response using [Camel Jolt].
And, we convert this response into the Java object.

## Deploy

You can use ServiceMix:

1. Download Apache ServiceMix (default assembly).
1. Unzip the server.
1. Define the `SERVICEMIX_HOME` variable.
1. Run `smx-start.bat` to start the server.
1. Run `smx-config.bat` to configure the server.
1. Run `app-deploy.bat` to deploy the application.
1. Run `app-test.bat` to test the application.

  [Github]: <http://github.com/>

  [Java EE]: <http://docs.oracle.com/javaee/>
  [JAX-RS]: <https://jcp.org/aboutJava/communityprocess/final/jsr339/index.html>
  [Jackson JAX-RS]: <https://github.com/FasterXML/jackson-jaxrs-providers>

  [WildFly]: <http://wildfly.org/>
  [Undertow]: <http://undertow.io/>
  [ServiceMix]: <http://servicemix.apache.org/>

  [Spring]: <http://spring.io/>

  [Jackson CSV]: <https://github.com/FasterXML/jackson-dataformat-csv>
  [Jolt]: <https://github.com/bazaarvoice/jolt>

  [Camel]: <http://camel.apache.org/>
  [Camel CXF]: <http://camel.apache.org/cxfrs.html>
  [Camel Jackson]: <http://camel.apache.org/json.html>
  [Camel Jolt]: <http://camel.apache.org/jolt.html>
